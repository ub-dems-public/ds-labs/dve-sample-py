---
title: Project Notes
subtitle: index to internal notes
caption: (dir)
author: --
date: 2021-10-29
---
|                                        |                         | 
| -------------------------------------- | ----------------------- |
| [Next: Project Usage](usage/README.md) | [Up: Top](../README.md) |

PROJECT NOTES
=============


Index
-----

* [Project Usage Guide](usage/README.md)
* [Project Customization](custom/README.org)


