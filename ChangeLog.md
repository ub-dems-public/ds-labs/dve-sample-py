# Changelog

All notable changes to this project will be documented in this file.

Based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [v0.1.0/m0] - 2019-10-28

### Added

- pyenv config with Python 3.6.9
- pipenv config with pytorch

### Changed

- doc/setup.md python env install guide

### Removed

- makefile
- docker support

## [v2.0.0/a1] - 2021-06-24

### Added

- pyenv config with Python 3.9.5
- poetry environment

### Changed

- project layout

### Removed

- pipenv support
- setup.py replaced by setup.cfg
